FROM golang:1.15
WORKDIR /app
COPY . /app
RUN make

FROM alpine:latest
COPY --from=0 /app/woodhouse /bin/woodhouse
RUN apk add --no-cache git
CMD ["/bin/woodhouse"]
