package main

import (
	"context"
	"fmt"
	"io"
	humanlog "log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/gorilla/mux"
	"github.com/oklog/run"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/version"
	"github.com/slack-go/slack"
	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackapps"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackutil"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/web"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	////////////////////
	// Config
	////////////////////

	app = kingpin.New("woodhouse", "Assorted SRE tools.")

	logFormat = app.Flag("log-format", "Format in which to print structured logs. Valid values: json, logfmt (default).").
			Envar("LOG_FORMAT").Default("logfmt").String()
	pushGatewayURL = app.Flag("pushgateway-url", "Prometheus pushgateway to push metrics to from cronjobs.").
			Envar("PUSHGATEWAY_URL").Default("").String()

	serveCmd   = app.Command("serve", "Start HTTP server, for Slack apps and other HTTP things.")
	listenAddr = serveCmd.Flag("listen-address", "Address to listen on.").
			Short('l').Envar("LISTEN_ADDRESS").Default(":8080").String()
	metricsListenAddr = serveCmd.Flag("metrics-listen-address", "Address to listen on.").
				Envar("METRICS_LISTEN_ADDRESS").Default(":8081").String()
	slackSigningSecret = serveCmd.Flag("slack-signing-secret", "Signing secret used to validate that requests originate from Slack.").
				Envar("SLACK_SIGNING_SECRET").Required().String()
	slackBotAccessToken = serveCmd.Flag("slack-bot-access-token", "Access token for the app's bot user to access the Slack API.").
				Envar("SLACK_BOT_ACCESS_TOKEN").Required().String()
	reqBodyMaxSize = serveCmd.Flag("http-request-body-max-size-bytes", "Maximum HTTP request body size in bytes.").
			Envar("HTTP_REQUEST_BODY_MAX_SIZE_BYTES").Default("1048576").Int64()
	maxReqsPerSecond = serveCmd.Flag("max-requests-per-second-per-ip", "Request rate limit per IP.").
				Envar("MAX_REQUESTS_PER_SECOND_PER_IP").Default("100").Int()
	gitlabAPIToken = serveCmd.Flag("gitlab-api-token", "GitLab API token to use for creating issues.").
			Envar("GITLAB_API_TOKEN").Required().String()
	gitlabAPIBaseURL = serveCmd.Flag("gitlab-api-base-url", "GitLab API to use for creating issues.").
				Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()
	gitlabIncidentIssueProjectPath = serveCmd.Flag("gitlab-incident-issue-project-path", "GitLab project path for incident issues.").
					Envar("GITLAB_INCIDENT_ISSUE_PROJECT_PATH").Required().String()
	gitlabWebhookToken = serveCmd.Flag("gitlab-webhook-token", "GitLab webhook secret token, to validate requests.").
				Envar("GITLAB_WEBHOOK_TOKEN").Required().String()

	// We could just inject the name, as the Slack API's sendMessage method
	// accepts channel names. However, this prevents us from linking to the
	// channel in replies to the invoking user. You need a channel ID for this,
	// and incredibly, the only way to get it is to list all conversations and
	// paginate through the results. This is too much hassle to be worth it, for a
	// parameter we'll rarely configure.
	incidentSlackChannelID = serveCmd.Flag("incident-slack-channel", "Slack channel in which to post incident updates. Obtain the ID by copying a link to the channel, and taking the last part of the path.").
				Envar("INCIDENT_SLACK_CHANNEL").Required().String()

	// Set these to enable real pages to be generated when declaring incidents.
	pagerdutyIntegrationKeyEOC = serveCmd.Flag("pagerduty-integration-key-eoc", "Pagerduty events V2 API integration key for the Production service (pages the EOC).").
					Envar("PAGERDUTY_INTEGRATION_KEY_EOC").Default("").String()
	pagerdutyIntegrationKeyIMOC = serveCmd.Flag("pagerduty-integration-key-imoc", "Pagerduty events V2 API integration key for the IMOC service.").
					Envar("PAGERDUTY_INTEGRATION_KEY_IMOC").Default("").String()
	pagerdutyIntegrationKeyCMOC = serveCmd.Flag("pagerduty-integration-key-cmoc", "Pagerduty events V2 API integration key for the CMOC service.").
					Envar("PAGERDUTY_INTEGRATION_KEY_CMOC").Default("").String()

	// This timeout is a bit of a lie: it's configured for the server's read and
	// write timeouts, and also set on the request context. In practice, requests
	// might actually be able to live for up to double this time.
	reqMaxDuration = serveCmd.Flag("http-request-max-duration-seconds", "Maximum HTTP request duration in seconds.").
			Envar("HTTP_REQUEST_MAX_DURATION_SECONDS").Default("30").Int64()

	// Useful for kubernetes. When a pod is terminated, it is asynchronously
	// removed from the service load balancer. During that time, HTTP requests can
	// still be sent to the pod.
	shutdownSleepSeconds = serveCmd.Flag("shutdown-sleep-seconds", "Time to sleep on receipt of a signal before waiting for all requests to complete, then shutting down.").
				Envar("SHUTDOWN_SLEEP_SECONDS").Default("0").Int()

	slackCmd                           = app.Command("slack", "Perform tasks on Slack.")
	slackArchiveIncidentChannelsCmd    = slackCmd.Command("archive-incident-channels", "Archive old incident channels.")
	archiveIncidentChannelsAccessToken = slackArchiveIncidentChannelsCmd.Flag("access-token", "Access token for the app's bot user to access the Slack API.").
						Envar("SLACK_BOT_ACCESS_TOKEN").Required().String()
	archiveIncidentChannelsMaxAge = slackArchiveIncidentChannelsCmd.Flag("max-age-days", "Archive incident slack channels older than this many days").
					Envar("INCIDENT_CHANNEL_MAX_AGE").Default("14").Int()
	archiveIncidentChannelsPrefix = slackArchiveIncidentChannelsCmd.Flag("name-prefix", "Name prefix for incident slack channels.").
					Envar("INCIDENT_CHANNEL_NAME_PREFIX").Required().String()

	gitlabCmd                   = app.Command("gitlab", "Perform tasks on GitLab.")
	gitlabNotifyMirroredMrCmd   = gitlabCmd.Command("notify-mirrored-mr", "When run inside a mirror's CI job, writes a note on any corresponding merge requests on the mirror source.")
	gitlabNotifyMirroredMrToken = gitlabNotifyMirroredMrCmd.Flag("gitlab-api-token", "GitLab API token to use for creating notes.").
					Envar("GITLAB_API_TOKEN").Required().String()
	gitlabNotifyMirroredMrBaseURL = gitlabNotifyMirroredMrCmd.Flag("gitlab-api-base-url", "GitLab API to use for creating issues.").
					Envar("GITLAB_API_BASE_URL").Default("https://gitlab.com/api/v4").String()
	gitlabNotifyMirroredMrProjectPath = gitlabNotifyMirroredMrCmd.Flag("project-path", "Project path of mirror source, if different from the mirror target's path.").
						Envar("GITLAB_MIRROR_PATH").Default("").String()
	gitlabNotifyMirroredMrPipelineStatus = gitlabNotifyMirroredMrCmd.Flag("pipeline-status", "Status of the mirror's pipeline.").
						Envar("GITLAB_MIRROR_PIPELINE_STATUS").Default("starting").String()

	versionCmd = app.Command("version", "Print build information.")

	////////////////////
	// Globals
	////////////////////

	logger log.Logger
)

func main() {
	cmd := kingpin.MustParse(app.Parse(os.Args[1:]))

	logger = createLogger(log.NewSyncWriter(os.Stdout))
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = level.NewInjector(logger, level.InfoValue())

	switch cmd {
	case serveCmd.FullCommand():
		fatal(serve())
	case slackArchiveIncidentChannelsCmd.FullCommand():
		fatal(archiveIncidentChannels())
	case gitlabNotifyMirroredMrCmd.FullCommand():
		fatal(notifyMirroredMr())
	case versionCmd.FullCommand():
		fatal(printVersion())
	default:
		fatal(fmt.Errorf("cmd %s unrecognized", cmd))
	}
}

func serve() error {
	logger.Log("event", "server_start")

	slackHandler := func(handler http.Handler) http.Handler {
		return slackapps.NewSlackHTTPMiddleware(*slackSigningSecret, handler)
	}

	reqTimeout := time.Duration(*reqMaxDuration) * time.Second
	reqGauge := new(sync.WaitGroup)
	jobRunner := &woodhouse.AsyncJobRunner{
		JobTimeout:  5 * time.Minute,
		Logger:      logger,
		ThreadCount: reqGauge,
	}

	httpClient := newHTTPClient()
	slackClient := slack.New(*slackBotAccessToken, slack.OptionHTTPClient(httpClient))

	pager := &pagerduty.Pager{HTTPClient: httpClient}
	gitlabClient := newGitLabClient(*gitlabAPIToken, *gitlabAPIBaseURL, httpClient)

	httpRouter := mux.NewRouter()
	httpRouter.HandleFunc("/ready", func(w http.ResponseWriter, req *http.Request) {
		fmt.Fprintln(w, "OK")
	}).Methods(http.MethodGet)

	slashCmdHandler := slackapps.NewSlashSubcommandDelegator()
	slashCmdHandler.HandleSlashSubcommand("echo", &slackapps.EchoSlashCommand{})
	slashCmdHandler.HandleSlashSubcommand("incident", &slackapps.IncidentSlashCommand{
		SlackClient: slackClient, JobRunner: jobRunner, IncidentChannelID: *incidentSlackChannelID,
	})
	httpRouter.Handle("/slack/slash", slackHandler(slashCmdHandler)).Methods(http.MethodPost)

	interactivityHandler := slackapps.NewInteractivityHandler()
	interactivityHandler.HandleInteraction(slackapps.IncidentDeclareCallbackID, &slackapps.IncidentModalHandler{
		JobRunner:                      jobRunner,
		SlackClient:                    slackClient,
		GitlabClient:                   gitlabClient,
		GitlabIncidentIssueProjectPath: *gitlabIncidentIssueProjectPath,

		Pager:                       pager,
		PagerdutyIntegrationKeyEOC:  *pagerdutyIntegrationKeyEOC,
		PagerdutyIntegrationKeyIMOC: *pagerdutyIntegrationKeyIMOC,
		PagerdutyIntegrationKeyCMOC: *pagerdutyIntegrationKeyCMOC,
	})
	httpRouter.Handle("/slack/interactivity", slackHandler(interactivityHandler)).Methods(http.MethodPost)

	gitlabIncidentWebhookHandler := &gitlab.IncidentIssueWebhookHandler{
		SlackClient: slackClient, ChannelID: *incidentSlackChannelID,
	}
	httpRouter.Handle("/gitlab/incident-issues", gitlab.NewGitLabWebhookMiddleware(
		*gitlabWebhookToken, gitlabIncidentWebhookHandler,
	)).Methods(http.MethodPost)

	httpHandler := web.NewRequestCountingMiddleware(
		reqGauge, instrumentation.NewInstrumentedHTTPMiddleware(
			logger, web.NewPanicRecoverMiddleware(
				handleWebPanic, web.NewLimitsMiddleware(
					reqTimeout, *reqBodyMaxSize, *maxReqsPerSecond, httpRouter,
				),
			),
		),
	)

	prometheus.MustRegister(version.NewCollector("woodhouse"))

	httpSocket, err := net.Listen("tcp", *listenAddr)
	fatal(err)
	httpServer := &http.Server{
		Handler:      httpHandler,
		ReadTimeout:  reqTimeout,
		WriteTimeout: reqTimeout,
	}

	metricsSocket, err := net.Listen("tcp", *metricsListenAddr)
	fatal(err)
	metricsServer := &http.Server{
		Handler:      promhttp.Handler(),
		ReadTimeout:  reqTimeout,
		WriteTimeout: reqTimeout,
	}

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGTERM, syscall.SIGINT)

	var threads run.Group
	threads.Add(func() error {
		return httpServer.Serve(httpSocket)
	}, func(error) {
		httpServer.Close()
	})
	threads.Add(func() error {
		return metricsServer.Serve(metricsSocket)
	}, func(error) {
		metricsServer.Close()
	})
	threads.Add(func() error {
		<-signals
		logger.Log("event", "signal_received")
		time.Sleep(time.Duration(*shutdownSleepSeconds) * time.Second)
		reqGauge.Wait()
		return nil
	}, func(error) {
	})
	return threads.Run()
}

func archiveIncidentChannels() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*15)
	defer cancel()

	slackClient := slack.New(*archiveIncidentChannelsAccessToken, slack.OptionHTTPClient(newHTTPClient()))
	logger := humanlog.New(os.Stdout, "[archive-incident channels] ", humanlog.LstdFlags)

	monitor := woodhouse.NewJobMonitor(prometheus.NewRegistry(), *pushGatewayURL)
	run := func(ctx context.Context) error {
		return slackutil.ArchiveIncidentChannels(ctx, logger, slackClient, *archiveIncidentChannelsPrefix, time.Duration(*archiveIncidentChannelsMaxAge)*24*time.Hour)
	}
	return monitor.Run(ctx, "archive_incident_channels", time.Minute*15, run)
}

func notifyMirroredMr() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*2)
	defer cancel()
	logger := humanlog.New(os.Stdout, "[notify-mirrored-mr] ", humanlog.LstdFlags)
	client := newGitLabClient(*gitlabNotifyMirroredMrToken, *gitlabNotifyMirroredMrBaseURL, newHTTPClient())
	return gitlab.NotifyMergeRequestFromMirrorSource(ctx, client, logger, *gitlabNotifyMirroredMrProjectPath, *gitlabNotifyMirroredMrPipelineStatus)
}

func printVersion() error {
	fmt.Println(version.Print("woodhouse"))
	return nil
}

func newGitLabClient(token, baseURL string, httpClient *http.Client) *gogitlab.Client {
	client, err := gogitlab.NewClient(
		token, gogitlab.WithBaseURL(baseURL),
		gogitlab.WithoutRetries(), // Use external retries, for better observability into how many failures occur
		gogitlab.WithHTTPClient(httpClient),
	)
	fatal(err)
	return client
}

func newHTTPClient() *http.Client {
	return &http.Client{Timeout: time.Second * 30}
}

func createLogger(w io.Writer) log.Logger {
	switch *logFormat {
	case "logfmt":
		return log.NewLogfmtLogger(w)
	case "json":
		return log.NewJSONLogger(w)
	default:
		// We can't call fatal without a logger!
		fmt.Printf("unsupported log format: %s\n", *logFormat)
		os.Exit(1)
		return nil
	}
}

func handleWebPanic(panicked interface{}, req *http.Request) {
	stackBuff := make([]byte, 8*1024)
	stackBytesWritten := runtime.Stack(stackBuff, false)
	stackBuff = stackBuff[0:stackBytesWritten]

	event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
	event.With("error", panicked, "stack", string(stackBuff), "message", "caught panic")
}

func fatal(err error) {
	if err != nil {
		level.Error(logger).Log("event", "shutdown", "error", err)
		os.Exit(1)
	}
}
