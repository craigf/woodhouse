# woodhouse

A monolithic project for GitLab infrastructure team tools. Slack apps, CLI
subcommands, and other assorted things.

## Functionality

- Slack apps
  - Incident management (coming soon)
  - On-call handover (coming soon)
- Commands (which may be CI-scheduled)
  - Alertmanager silence issue updates (Helicopter) (coming soon)
  - On-call report generator (coming soon)

## Developing

Build woodhouse:

```
go build ./cmd/woodhouse
```

You can use [ngrok](https://ngrok.com/) to expose a locally-running web server
on the public internet.

```
ngrok http --region <region> 8080
```

See the available configuration:

```
./woodhouse --help
```

Or look at the flags in [`main.go`](cmd/woodhouse/main.go).

Follow the "Installing Slack App" instructions below to install a Slack app on a
Slack workspace you control. When configuring the callback URLs, use the ngrok
URL given to you by `ngrok http` as a base.

We might eventually create a staging deployment to test out branch builds on the
GitLab slack, but at the moment this is the only way to test changes to
Woodhouse.

At a minimum, set:

```
export SLACK_SIGNING_SECRET=<from the Slack app config>
export SLACK_BOT_ACCESS_TOKEN=<from the Slack app config>
export GITLAB_API_TOKEN=<API token to test incident issue creation with>
export GITLAB_INCIDENT_ISSUE_PROJECT_PATH=<a gitlab.com project path to test incident issue creation with>
```

You can use [`direnv`](https://direnv.net/) to automate the sourcing of a
`.envrc` file if you want.

Start woodhouse:

```
./woodhouse serve
```

You can now run Slack slash commands like `/woodhouse incident declare`, or
`/woodhouse echo`.

## Operating

### Installing Slack App

1. Visit https://api.slack.com/apps.
1. "Create New App". Give it a name and a development workspace.
1. If Slack offers to create a bot user for you, do it. Otherwise make sure you
   configure a bot user later.
1. Configure a slash command, `/woodhouse`.
   1. Set the request URL to wherever you will deploy Woodhouse, with a path of
      `/slack/slash`.
   1. Usage hint: `help|incident`
1. Enable interactivity (in the features sidebar).
   1. Request URL: `<woodhouse domain>/slack/interactivity`
1. Navigate to the OAuth & Permissions page.
   1. Grab the bot user OAuth token. You'll need to feed this to Woodhouse.
   1. Under bot token scopes, add `chat:write`, `channels:manage`, and
      `channels:read`.
1. Navigate to the Basic Information tab.
   1. Grab the signing secret for this app. You'll need to feed this to
      Woodhouse.

## Maintainers

- [Craig Furman](https://gitlab.com/craigf)

## Why put unrelated things into the same codebase / executable?

The main benefits are:

- One language (Go). This benefit is a double-edged sword, but:
  - Go is a language many on the GitLab infrastructure team have at least some
    familiarity with.
  - Code can be shared between the different components easily.
  - We're unlikely to make frequent changes to many of these components, and if
    some were written in different languages, the mean time between working in
    that language might be quite long, making it difficult to quickly jump in
    and make a change.
- New components don't have to reinvent the "project core" wheel: event logging,
  metrics, HTTP middleware (e.g. Slack authorization). If the codebase already
  contains a Slack app, or a CLI subcommand, slotting in new functionality
  should be simple.
- No need to set up build, test, and deployment pipelines for many small tools.
- One place to keep dependencies up to date.

The main drawbacks are:

- When making a change to a component, you and/or a CI job must build/index
  code, and run tests, for code you're not using.
- The built artifacts are larger than they would be for any one component
  extracted into its own codebase.

These drawbacks are most noticeable at large scale, which this project is
unlikely to reach.

Most of the benefits could be realized in a monorepo codebase that builds
several binaries, but the build and deployment pipelines are maximally simple
with the monolithic binary approach.
