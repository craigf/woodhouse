module gitlab.com/gitlab-com/gl-infra/woodhouse

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/go-retryablehttp v0.6.7 // indirect
	github.com/iancoleman/strcase v0.1.2
	github.com/oklog/run v1.1.0
	github.com/prometheus/client_golang v1.7.1
	github.com/prometheus/common v0.14.0
	github.com/prometheus/procfs v0.2.0 // indirect
	github.com/rs/xid v1.2.1
	github.com/sethvargo/go-retry v0.1.0
	github.com/slack-go/slack v0.6.6
	github.com/stretchr/testify v1.4.0
	github.com/xanzy/go-gitlab v0.38.1
	golang.org/x/net v0.0.0-20200927032502-5d4f70055728 // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	golang.org/x/sys v0.0.0-20200929083018-4d22bbb62b3c // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e
	gopkg.in/alecthomas/kingpin.v2 v2.2.6
)
