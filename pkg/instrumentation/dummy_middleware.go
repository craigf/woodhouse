package instrumentation

import (
	"context"
	"net/http"
)

// A dummy middleware that inserts an event into the context then does nothing
// with it. Useful for testing other application middlewares that assume an
// event is present.
func NewDummyInstrumentationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		event := &Event{}
		reqWithEvent := req.Clone(context.WithValue(req.Context(), EventContextKey{}, event))
		next.ServeHTTP(w, reqWithEvent)
	})
}
