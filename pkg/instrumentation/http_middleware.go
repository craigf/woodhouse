package instrumentation

import (
	"context"
	"net/http"

	"github.com/go-kit/kit/log"
)

func NewInstrumentedHTTPMiddleware(logger log.Logger, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		event := &Event{}
		reqWithEvent := req.Clone(context.WithValue(req.Context(), EventContextKey{}, event))
		respStatusSpy := &responseWriterSpy{ResponseWriter: w}

		duration := MeasureTime(func() {
			next.ServeHTTP(respStatusSpy, reqWithEvent)
		})

		status := respStatusSpy.statusCode
		if status == 0 {
			status = http.StatusOK
		}
		event.With("event", "http_request", "status", status, "duration_s", duration, "path", req.URL.Path, "method", req.Method)
		logger.Log(*event...)
	})
}

type EventContextKey struct{}

type responseWriterSpy struct {
	statusCode int
	http.ResponseWriter
}

func (w *responseWriterSpy) WriteHeader(statusCode int) {
	w.statusCode = statusCode
	w.ResponseWriter.WriteHeader(statusCode)
}
