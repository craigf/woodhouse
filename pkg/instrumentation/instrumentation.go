package instrumentation

import "time"

type Event []interface{}

func (e *Event) With(keysAndVals ...interface{}) {
	*e = append(*e, keysAndVals...)
}

func MeasureTime(op func()) float64 {
	start := time.Now()
	op()
	return float64(time.Since(start)) / float64(time.Second)
}
