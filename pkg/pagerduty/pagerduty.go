package pagerduty

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/rs/xid"
	"github.com/sethvargo/go-retry"
)

type Pager struct {
	HTTPClient *http.Client
}

func (p *Pager) Page(ctx context.Context, integrationKey, message string) error {
	if integrationKey == "" {
		return errors.New("Integration key not set for this service")
	}

	hostname, err := os.Hostname()
	if err != nil {
		return err
	}

	dedupKey := xid.New().String()

	body, err := json.Marshal(PagerdutyEvent{
		RoutingKey:  integrationKey,
		DedupKey:    dedupKey,
		EventAction: "trigger",
		Payload: PagerdutyEventPayload{
			Summary:  message,
			Source:   hostname,
			Severity: "info",
		},
	})
	if err != nil {
		return err
	}

	return retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		resp, err := p.HTTPClient.Post("https://events.pagerduty.com/v2/enqueue", "application/json", bytes.NewReader(body))
		if err != nil {
			return retry.RetryableError(err)
		}
		defer resp.Body.Close()
		if resp.StatusCode != 202 {
			return retry.RetryableError(fmt.Errorf("expected HTTP status 202, got %d", resp.StatusCode))
		}
		return nil
	})
}

type PagerdutyEvent struct {
	RoutingKey  string                `json:"routing_key"`
	DedupKey    string                `json:"dedup_key"`
	EventAction string                `json:"event_action"`
	Payload     PagerdutyEventPayload `json:"payload"`
}

type PagerdutyEventPayload struct {
	Summary  string `json:"summary"`
	Source   string `json:"source"`
	Severity string `json:"severity"`
}
