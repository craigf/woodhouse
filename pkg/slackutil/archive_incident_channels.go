package slackutil

import (
	"context"
	"log"
	"strings"
	"time"

	"github.com/slack-go/slack"
)

func ArchiveIncidentChannels(ctx context.Context, logger *log.Logger, client *slack.Client, namePrefix string, maxAge time.Duration) error {
	now := time.Now().UTC()
	listParams := &slack.GetConversationsParameters{ExcludeArchived: "true"}
	for {
		channels, nextCursor, err := client.GetConversationsContext(ctx, listParams)
		if err != nil {
			return err
		}
		for _, channel := range channels {
			if strings.HasPrefix(channel.Name, namePrefix+"-") {
				createdAt := channel.Created.Time().UTC()
				log.Printf("Found channel %s, created at %s", channel.Name, createdAt.String())
				if now.Sub(createdAt) > maxAge {
					log.Println("Archiving " + channel.Name)
					if err := client.ArchiveConversationContext(ctx, channel.ID); err != nil {
						return err
					}
				}
			}
		}
		if nextCursor == "" {
			return nil
		}
		listParams.Cursor = nextCursor
	}
}
