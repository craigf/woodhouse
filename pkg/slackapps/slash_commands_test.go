package slackapps_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"github.com/slack-go/slack"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/slackapps"
)

func TestSlashCommandHandler(t *testing.T) {
	for _, tc := range []struct {
		name                 string
		command              string
		expectedResponse     string
		expectedHandlerCalls int
	}{
		{
			name:                 "calls handler for first word of space-separated slash command text",
			command:              "test%20this%20is%20a%20test",
			expectedHandlerCalls: 1,
		},
		{
			name:                 "calls handler for first word of any-whitespace-separated slash command text",
			command:              "test%20%20%20%0Athis%20is%20a%20test",
			expectedHandlerCalls: 1,
		},
		{
			name:                 "returns error text when slash subcommand not found",
			command:              "wrong%20this%20is%20a%20test",
			expectedResponse:     "error: no slash command handler found for subcommand 'wrong'",
			expectedHandlerCalls: 0,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			handler := slackapps.NewSlashSubcommandDelegator()

			handlerCallCount := 0
			handlerFunc := func(
				event *instrumentation.Event, command slack.SlashCommand,
				w http.ResponseWriter, req *http.Request,
			) {
				handlerCallCount++
				fmt.Fprint(w, command.Text)
			}

			handler.HandleSlashSubcommand("test", &fakeSlashCommandHandler{handler: handlerFunc})

			server := httptest.NewServer(instrumentation.NewDummyInstrumentationMiddleware(handler))
			defer server.Close()

			req, err := http.NewRequest(http.MethodPost, server.URL, strings.NewReader(fmt.Sprintf(slashCommandBody, tc.command)))
			require.Nil(t, err)
			req.Header.Set("Content-type", "application/x-www-form-urlencoded")
			resp, err := server.Client().Do(req)
			require.Nil(t, err)
			defer resp.Body.Close()
			require.Equal(t, http.StatusOK, resp.StatusCode)
			respBody, err := ioutil.ReadAll(resp.Body)
			require.Nil(t, err)

			expectedResponse := tc.expectedResponse
			if expectedResponse == "" {
				unescapedText, err := url.QueryUnescape(tc.command)
				require.Nil(t, err)
				expectedResponse = unescapedText
			}

			assert.Equal(t, expectedResponse, strings.TrimSpace(string(respBody)))

			assert.Equal(t, tc.expectedHandlerCalls, handlerCallCount)
		})
	}
}

const (
	slashCommandBody = `token=gIkuvaNzQIHg97ATvDxqgjtO
&team_id=T0001
&team_domain=example
&enterprise_id=E0001
&enterprise_name=Globular%%20Construct%%20Inc
&channel_id=C2147483705
&channel_name=test
&user_id=U2147483697
&user_name=Steve
&command=/someSuperCommand
&text=%s
&response_url=https://hooks.slack.com/commands/1234/5678
&trigger_id=13345224609.738474920.8088930838d88f008e0
&api_app_id=A123456
`
)

type fakeSlashCommandHandler struct {
	handler func(
		event *instrumentation.Event, command slack.SlashCommand,
		w http.ResponseWriter, req *http.Request,
	)
}

func (s *fakeSlashCommandHandler) Handle(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	s.handler(event, command, w, req)
}
