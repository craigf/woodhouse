package slackapps

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strings"
	"text/template"
	"time"

	"github.com/sethvargo/go-retry"
	"github.com/slack-go/slack"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/pagerduty"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/woodhouse"
)

const IncidentDeclareCallbackID = "incident-declare"

type IncidentSlashCommand struct {
	SlackClient       *slack.Client
	JobRunner         *woodhouse.AsyncJobRunner
	IncidentChannelID string
}

type IncidentContext struct {
	Username          string
	AnnounceChannelID string
	MessageTimestamp  string
}

func (s *IncidentSlashCommand) Handle(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	subcommandArgs := whitespace.Split(command.Text, -1)
	if len(subcommandArgs) != 2 {
		s.help(event, command, w, req)
		return
	}

	switch subcommandArgs[1] {
	case "declare":
		s.declare(event, command, w, req)
	default:
		s.help(event, command, w, req)
	}
}

func (s *IncidentSlashCommand) declare(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	// echo the slash command back into the channel, but save the message content
	// for a "regular" bot user message below. We do this so that we can edit that
	// message later, which we can't do for direct slash command responses.
	if err := json.NewEncoder(w).Encode(slack.Msg{ResponseType: slack.ResponseTypeInChannel}); err != nil {
		event.With("error", err, "message", "error responding to slash command")
		return
	}

	s.JobRunner.RunAsyncJob(func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return s.presentIncidentModal(ctx, event, &command)
	})
}

func (s *IncidentSlashCommand) help(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	if err := json.NewEncoder(w).Encode(slack.Msg{
		ResponseType: slack.ResponseTypeEphemeral,
		Text:         "Usage: /woodhouse incident declare",
	}); err != nil {
		event.With("error", err, "message", "error writing HTTP response")
	}
}

func (s *IncidentSlashCommand) presentIncidentModal(ctx context.Context, event *instrumentation.Event, command *slack.SlashCommand) error {
	_, _, _, _ = s.SlackClient.SendMessageContext(
		ctx, command.ChannelID,
		slack.MsgOptionText(fmt.Sprintf("Declaring incident in <#%s>", s.IncidentChannelID), false),
	)

	_, msgTimestamp, _, err := s.SlackClient.SendMessageContext(
		ctx, s.IncidentChannelID,
		slack.MsgOptionText("Declaring an incident. Please wait for the caller to finish filling in the modal.", false),
	)
	if err != nil {
		return fmt.Errorf("error sending message: %s", err)
	}

	callbackData, err := json.Marshal(IncidentContext{
		Username:          command.UserName,
		AnnounceChannelID: s.IncidentChannelID,
		MessageTimestamp:  msgTimestamp,
	})
	if err != nil {
		return err
	}

	modal := slack.ModalViewRequest{
		Type:            slack.VTModal,
		Title:           plainText("Declare an incident"),
		Close:           plainText("Close"),
		Submit:          plainText("Submit"),
		CallbackID:      IncidentDeclareCallbackID,
		PrivateMetadata: string(callbackData),
	}

	titlePlaceholder := time.Now().UTC().Format("2006-01-02") + ": Errors reticulating splines..."
	titleBlock := textInputBlock("Title", titlePlaceholder)
	severityHelpLink := slack.NewTextBlockObject("mrkdwn", "<https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity|Handbook: when to use each severity label>", false, false)
	severityHelpBlock := slack.NewSectionBlock(severityHelpLink, nil, nil)
	severityBlock := selectInputBlock("Severity", "Choose a severity", "severity::1", "severity::2", "severity::3", "severity::4")
	tasksBlock := slack.NewInputBlock(
		"tasks", plainText("Tasks"),
		slack.NewCheckboxGroupsBlockElement(
			"tasks",
			slack.NewOptionBlockObject("pageEOC", plainText("Page on-call engineer")),
			slack.NewOptionBlockObject("pageIMOC", plainText("Page IMOC (for use by on-call engineers only)")),
			slack.NewOptionBlockObject("pageCMOC", plainText("Page CMOC (for use by on-call engineers only)")),
		),
	)
	tasksBlock.Optional = true

	modal.Blocks.BlockSet = append(modal.Blocks.BlockSet, titleBlock, severityHelpBlock, severityBlock, tasksBlock)

	_, err = s.SlackClient.OpenView(command.TriggerID, modal)
	if err != nil {
		return fmt.Errorf("error opening modal: %s", err)
	}
	return nil
}

type IncidentModalHandler struct {
	JobRunner                      *woodhouse.AsyncJobRunner
	SlackClient                    *slack.Client
	GitlabClient                   *gitlab.Client
	GitlabIncidentIssueProjectPath string

	Pager                       *pagerduty.Pager
	PagerdutyIntegrationKeyEOC  string
	PagerdutyIntegrationKeyIMOC string
	PagerdutyIntegrationKeyCMOC string
}

func (i *IncidentModalHandler) Handle(
	event *instrumentation.Event, payload slack.InteractionCallback,
	w http.ResponseWriter, req *http.Request,
) {
	i.JobRunner.RunAsyncJob(func(ctx context.Context) error {
		event := ctx.Value(instrumentation.EventContextKey{}).(*instrumentation.Event)
		return i.createIncident(ctx, event, &payload)
	})
}

func (i *IncidentModalHandler) createIncident(ctx context.Context, event *instrumentation.Event, payload *slack.InteractionCallback) error {
	event.With("job", "create_incident")

	var incident IncidentContext
	if err := json.Unmarshal([]byte(payload.View.PrivateMetadata), &incident); err != nil {
		return err
	}

	updateMsg := func(msg string) error {
		_, _, _, err := i.SlackClient.SendMessageContext(
			ctx, incident.AnnounceChannelID,
			slack.MsgOptionText(msg, false),
			slack.MsgOptionUpdate(incident.MessageTimestamp),
		)
		return err
	}

	appendToThread := func(msg string) error {
		_, _, _, err := i.SlackClient.SendMessageContext(
			ctx, incident.AnnounceChannelID,
			slack.MsgOptionText(msg, false),
			slack.MsgOptionTS(incident.MessageTimestamp),
			slack.MsgOptionBroadcast(),
		)
		return err
	}

	if err := updateMsg(":loading: Creating incident issue..."); err != nil {
		return err
	}

	issue, err := i.createIssue(ctx, event, payload, incident)
	if err != nil {
		_ = updateMsg(":x: Incident issue creation failed: " + err.Error())
		return err
	}
	if err := updateMsg(":white_check_mark: Incident issue created: " + issue.WebURL); err != nil {
		return err
	}

	incidentChannel, err := i.createChannel(ctx, issue)
	if err != nil {
		_ = appendToThread(fmt.Sprintf("error creating incident channel: %s", err))
		return err
	}
	_ = appendToThread(fmt.Sprintf("Slack channel for incident: <#%s>", incidentChannel.ID))

	// Don't fail the job on these optional tasks, but do notify the slack thread
	// and log the errors.
	taskErrs := i.performOptionalTasks(ctx, payload, incident, appendToThread)
	for i, err := range taskErrs {
		event.With(fmt.Sprintf("optional_task_err_%d", i), err)
		_ = appendToThread(err.Error())
	}

	return nil
}

func (i *IncidentModalHandler) createIssue(
	ctx context.Context, event *instrumentation.Event,
	payload *slack.InteractionCallback, incident IncidentContext,
) (*gitlab.Issue, error) {
	title := payload.View.State.Values["title"]["title"].Value
	severity := payload.View.State.Values["severity"]["severity"].SelectedOption.Value

	var description bytes.Buffer
	descTmpl := template.Must(template.New("description").Parse(incidentDescriptionTemplate))
	err := descTmpl.Execute(&description, struct {
		Date, Time, Username string
	}{
		Date:     time.Now().UTC().Format("2006-01-02"),
		Time:     time.Now().UTC().Format("15:04"),
		Username: incident.Username,
	})
	if err != nil {
		return nil, err
	}
	descStr := description.String()

	var issue *gitlab.Issue
	var issueCreateErrs []string
	err = retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		var err error
		issue, _, err = i.GitlabClient.Issues.CreateIssue(i.GitlabIncidentIssueProjectPath, &gitlab.CreateIssueOptions{
			Title:       &title,
			Description: &descStr,
			Labels:      gitlab.Labels{"Source::IMA::IncidentDeclare", "incident", "Incident::Active", severity},
		}, gitlab.WithContext(ctx))
		if err != nil {
			issueCreateErrs = append(issueCreateErrs, err.Error())
		}
		return retry.RetryableError(err)
	})
	if err != nil {
		event.With("issue_create_errs", strings.Join(issueCreateErrs, "\n"))
	}
	return issue, err
}

func (i *IncidentModalHandler) createChannel(ctx context.Context, issue *gitlab.Issue) (*slack.Channel, error) {
	projectPathParts := strings.Split(i.GitlabIncidentIssueProjectPath, "/")
	projectName := projectPathParts[len(projectPathParts)-1]
	incidentChannelName := fmt.Sprintf("%s-%d", projectName, issue.IID)
	incidentChannel, err := i.SlackClient.CreateConversationContext(ctx, incidentChannelName, false)
	if err != nil {
		return nil, fmt.Errorf("error creating slack channel: %s", err)
	}

	channelPurpose := fmt.Sprintf(`For discussion of incident "%s": %s`, issue.Title, issue.WebURL)
	_, err = i.SlackClient.SetPurposeOfConversationContext(ctx, incidentChannel.ID, channelPurpose)
	if err != nil {
		return nil, fmt.Errorf("error setting channel purpose: %s", err)
	}
	_, err = i.SlackClient.SetTopicOfConversationContext(ctx, incidentChannel.ID, channelPurpose)
	if err != nil {
		return nil, fmt.Errorf("error setting channel topic: %s", err)
	}

	slackChannelIssueComment := fmt.Sprintf("Slack channel: [\\#%s](%s)", incidentChannelName, channelLink(incidentChannel.ID))
	err = retry.Exponential(ctx, time.Second, func(ctx context.Context) error {
		_, _, err := i.GitlabClient.Notes.CreateIssueNote(
			i.GitlabIncidentIssueProjectPath, issue.IID,
			&gitlab.CreateIssueNoteOptions{Body: &slackChannelIssueComment},
			gitlab.WithContext(ctx),
		)
		return retry.RetryableError(err)
	})
	if err != nil {
		return nil, fmt.Errorf("error linking channel in issue comment: %s", err)
	}
	return incidentChannel, nil
}

func (i *IncidentModalHandler) performOptionalTasks(
	ctx context.Context, payload *slack.InteractionCallback, incident IncidentContext,
	appendToThread func(string) error,
) []error {
	pagerMsg := fmt.Sprintf("Please see incident declaration in Slack channel: %s", channelLink(incident.AnnounceChannelID))
	var taskErrs []error
	for _, selectedTask := range payload.View.State.Values["tasks"]["tasks"].SelectedOptions {
		switch selectedTask.Value {
		case "pageEOC":
			_ = appendToThread("Paging on-call engineer")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyEOC, pagerMsg); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging EOC: %s", err))
			}
		case "pageIMOC":
			_ = appendToThread("Paging IMOC")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyIMOC, pagerMsg); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging IMOC: %s", err))
			}
		case "pageCMOC":
			_ = appendToThread("Paging CMOC")
			if err := i.Pager.Page(ctx, i.PagerdutyIntegrationKeyCMOC, pagerMsg); err != nil {
				taskErrs = append(taskErrs, fmt.Errorf("error paging CMOC: %s", err))
			}
		default:
			err := fmt.Errorf("unknown task: %s", selectedTask.Value)
			taskErrs = append(taskErrs, err)
		}
	}
	return taskErrs
}

const incidentDescriptionTemplate = `
<!-- ISSUE TITLING: use the form "YYYY-MM-DD: briefly describe problem" -->

<!-- ISSUE LABELING: Don't forget to add labels for severity (severity::1 - severity::4) and service. if the incident relates to sensitive data, or is security related use the label ~security and mark it confidential. -->

## Summary

<!--
Leave a brief headline remark so that people know what's going on. It's fine for
this to be vague while not much is known.
-->

Context will be added here as we investigate.

## Timeline

All times UTC.

{{ .Date }}

- {{ .Time }} - {{ .Username }} declares incident in Slack.

<!-- THE BELOW IS TO BE CONDUCTED ONCE THE ABOVE INCIDENT IS MITIGATED. TRANSFER DATA FROM THE ABOVE INTO THE INCIDENT REVIEW SECTIONS BELOW. -->
<br/>
<details>
<summary><i>Click to expand or collapse the Incident Review section.</i>
<br/>
<h2>Incident Review</h2>
</summary>

<!--
The purpose of this Incident Review is to serve as a classroom to help us better understand the root causes of an incident. Treating it as a classroom allows us to create the space to let us focus on devising the mechanisms needed to prevent a similar incident from recurring in the future. A root cause can **never be a person** and this Incident Review should be written to refer to the system and the context rather than the specific actors. As placeholders for names, consider the usage of nouns like "technician", "engineer on-call", "developer", etc..
-->

## Summary

<!--
_A brief summary of what happened. Try to make it as executive-friendly as possible._

_example: For a period of 19 minutes (between 2020-05-01 12:00 UTC and 2020-05-01 12:19 UTC), GitLab.com experienced a drop in traffic to the database. 507 customers saw 2,342 503 errors over this 19 minute period. The underlying cause has been determined to be a change to the PgBouncer configuration (https://gitlab.com/gitlab-com/gl-infra/production/-/issues/XXXX) which caused the total number of connections to be reduced to 50. This incident was then mitigated by rolling back this PgBouncer configuration change.
-->

1. Service(s) affected:
1. Team attribution:
1. Minutes downtime or degradation:

<!--
_For calculating duration of event, use the [Platform Metrics Dashboard](https://dashboards.gitlab.net/d/general-triage/general-platform-triage?orgId=1) to look at appdex and SLO violations._
-->

## Metrics

<!--
_Provide any relevant graphs that could help understand the impact of the incident and its dynamics._
-->

## Customer Impact

1. Who was impacted by this incident? (i.e. external customers, internal customers)
2. What was the customer experience during the incident? (i.e. preventing them from doing X, incorrect display of Y, ...)
3. How many customers were affected?
4. If a precise customer impact number is unknown, what is the estimated potential impact?

## Incident Response Analysis

1. How was the event detected?
2. How could detection time be improved?
3. How did we reach the point where we knew how to mitigate the impact?
4. How could time to mitigation be improved?

## Post Incident Analysis

1. How was the root cause diagnosed?
2. How could time to diagnosis be improved?
3. Do we have an existing backlog item that would've prevented or greatly reduced the impact of this incident?
4. Was this incident triggered by a change (deployment of code or change to infrastructure. _If yes, have you linked the issue which represents the change?_)?

## 5 Whys

<!--
_This section is meant to dig into lessons learned and corrective actions, it is not limited to 5 and consider how you may dive deeper into each why_

_example:_

1. Customers experienced an inability to create new projects on GitLab.com, why?
    - A code change was deployed which contained an escaped bug.
1. Why did this bug not get noticed in staging?
    - The integration test for this use case is missing.
1. Why is an integration test for this use case missing?
    - It was inadvertently removed during a refactoring of our test suite.
1. Why was the test suite being refactored?
    - As part of our efforts to decrease MTTP.
1. Why did it take 2 hours to notice this issue in production?
    - The initial alert was supressed as a false alarm.
1. Why was this alert suppressed
    - The system which dedupes alerts inadvertently suppressed this alarm as a duplicate.
1. Why did it take 4 hours to resolve the issue in production?
    - The change which carried this escaped bug also contained a database schema change which made rolling the change back impossible. Engineering was engaged immediately by the oncall SRE and conducted a forward fix.
-->

## Lessons Learned

<!--
_Be explicit about what lessons we learned and should carry forward. These usually inform what our corrective actions should be._

_example:_
1. The results of refactoring activites around our integration tests should be reviewed. (i.e we had 619 tests before refactor but 618 after.)
2. Our tooling to dedupe alarms should have integration tests to ensure it works against existing and newly added alarms.
-->

## Corrective Actions

<!--
- _Use Lessons Learned as a guideline for creation of Corrective Actions
- _List issues that have been created as corrective actions from this incident._
- _For each issue, include the following:_
    - _<Bare Issue link> - Issue labeled as ~"corrective action"._
    - _Include an estimated date of completion of the corrective action._
    - _Include the named individual who owns the delivery of the corrective action._
-->

## Guidelines

- [Blameless RCA Guideline](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/workflows/internal/root-cause-analysis.html#meeting-purpose)

</details>
`
