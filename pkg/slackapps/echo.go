package slackapps

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

type EchoSlashCommand struct{}

func (s *EchoSlashCommand) Handle(
	event *instrumentation.Event, command slack.SlashCommand,
	w http.ResponseWriter, req *http.Request,
) {
	subcommandArgs := whitespace.Split(command.Text, -1)
	text := strings.Join(subcommandArgs[1:], " ")

	if err := json.NewEncoder(w).Encode(slack.Msg{
		ResponseType: slack.ResponseTypeInChannel,
		Text:         fmt.Sprintf("<@%s> in <#%s> says: %s", command.UserID, command.ChannelID, text),
	}); err != nil {
		event.With("error", err, "message", "error writing HTTP response")
	}
}
