package slackapps

import (
	"github.com/iancoleman/strcase"
	"github.com/slack-go/slack"
)

func plainText(text string) *slack.TextBlockObject {
	return slack.NewTextBlockObject("plain_text", text, false, false)
}

func textInputBlock(title, placeholder string) *slack.InputBlock {
	id := strcase.ToLowerCamel(title)
	input := slack.NewPlainTextInputBlockElement(plainText(placeholder), id)
	return slack.NewInputBlock(id, plainText(title), input)
}

func selectInputBlock(title, placeholder string, options ...string) *slack.InputBlock {
	id := strcase.ToLowerCamel(title)
	var selectOpts []*slack.OptionBlockObject
	for _, option := range options {
		selectOpts = append(selectOpts, slack.NewOptionBlockObject(option, plainText(option)))
	}
	input := slack.NewOptionsSelectBlockElement("static_select", plainText(placeholder), id, selectOpts...)
	return slack.NewInputBlock(id, plainText(title), input)
}

func channelLink(channelID string) string {
	return "https://slack.com/app_redirect?channel=" + channelID
}
