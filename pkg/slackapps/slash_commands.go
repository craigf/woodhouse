package slackapps

import (
	"fmt"
	"net/http"
	"regexp"

	"github.com/slack-go/slack"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

var whitespace = regexp.MustCompile(`\s+`)

type SlashSubCommandDelegator struct {
	cmds map[string]SlashSubcommandHandler
}

func NewSlashSubcommandDelegator() *SlashSubCommandDelegator {
	return &SlashSubCommandDelegator{cmds: map[string]SlashSubcommandHandler{}}
}

type SlashSubcommandHandler interface {
	Handle(
		event *instrumentation.Event, command slack.SlashCommand,
		w http.ResponseWriter, req *http.Request,
	)
}

func (s *SlashSubCommandDelegator) HandleSlashSubcommand(command string, handler SlashSubcommandHandler) {
	s.cmds[command] = handler
}

func (s *SlashSubCommandDelegator) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)

	slash, err := slack.SlashCommandParse(req)
	if err != nil {
		event.With("error", err, "message", "error parsing slack command")
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	slashCommandArgs := whitespace.Split(slash.Text, -1)
	subcommand := slashCommandArgs[0]

	handler := s.cmds[subcommand]
	if handler == nil {
		err := fmt.Errorf("error: no slash command handler found for subcommand '%s'", subcommand)
		event.With("error", err)
		fmt.Fprintln(w, err)
		return
	}

	event.With("slash_subcommand", subcommand)

	// If we can parse the command at all, send an HTTP 200, even if we will
	// indicate that an error has occurred:
	// https://api.slack.com/interactivity/slash-commands#responding_with_errors
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)

	handler.Handle(event, slash, w, req)
}
