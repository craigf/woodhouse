package gitlab_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/gitlab"
)

func TestGitLabWebhookMiddleware(t *testing.T) {
	for _, tc := range []struct {
		name                     string
		reqToken                 string
		expectedStatus           int
		expectedHandlerCallCount int
	}{
		{
			name:                     "returns HTTP 401 without calling next handler when no GitLab header set",
			expectedStatus:           401,
			expectedHandlerCallCount: 0,
		},
		{
			name:                     "returns HTTP 401 without calling next handler when incorrect GitLab header set",
			reqToken:                 "wrong-token",
			expectedStatus:           401,
			expectedHandlerCallCount: 0,
		},
		{
			name:                     "Calls the next handler when correct GitLab header set",
			reqToken:                 "a-token",
			expectedStatus:           202,
			expectedHandlerCallCount: 1,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			handlerCallCount := 0
			handler := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
				handlerCallCount++
				w.WriteHeader(http.StatusAccepted)
			})

			middleware := gitlab.NewGitLabWebhookMiddleware("a-token", handler)
			server := httptest.NewServer(middleware)
			defer server.Close()

			req, err := http.NewRequest(http.MethodPost, server.URL, nil)
			require.Nil(t, err)
			if tc.reqToken != "" {
				req.Header.Set("X-Gitlab-Token", tc.reqToken)
			}

			resp, err := server.Client().Do(req)
			require.Nil(t, err)
			defer resp.Body.Close()
			assert.Equal(t, tc.expectedStatus, resp.StatusCode)
			assert.Equal(t, tc.expectedHandlerCallCount, handlerCallCount)
		})
	}
}
