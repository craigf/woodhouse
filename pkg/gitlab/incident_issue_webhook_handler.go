package gitlab

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/slack-go/slack"
	gogitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

type IncidentIssueWebhookHandler struct {
	SlackClient *slack.Client
	ChannelID   string
}

func (h *IncidentIssueWebhookHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	event := req.Context().Value(instrumentation.EventContextKey{}).(*instrumentation.Event)

	payloadBytes, err := ioutil.ReadAll(req.Body)
	if err != nil {
		event.With("error", err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	webhookEvent, err := gogitlab.ParseWebhook(gogitlab.WebhookEventType(req), payloadBytes)
	if err != nil {
		event.With("error", err)
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	issueEvent, ok := webhookEvent.(*gogitlab.IssueEvent)
	if !ok {
		event.With("error", "event is not an IssueEvent")
		w.WriteHeader(http.StatusUnprocessableEntity)
		return
	}

	postedUpdate := false

	// Implementation of
	// https://ops.gitlab.net/gitlab-com/gl-infra/incident-management/blob/master/incident-declare-handler.js#L222,
	// with one change: posts on update action removed. This is very noisy in
	// practice! This will be revisited in the SRE survey (https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11262)
	if issueEvent.ObjectAttributes.Action == "open" && labelsInclude(issueEvent.Labels, "incident") && !labelsInclude(issueEvent.Labels, "Source::IMA::IncidentDeclare") {
		err = h.postMessage(req.Context(), issueEvent)
		postedUpdate = true
	} else if issueEvent.ObjectAttributes.Action == "reopen" && labelsInclude(issueEvent.Labels, "Incident::Active") {
		err = h.postMessage(req.Context(), issueEvent)
		postedUpdate = true
	}

	event.With("posted_update", postedUpdate)
	if err != nil {
		event.With("error", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (h *IncidentIssueWebhookHandler) postMessage(ctx context.Context, issue *gogitlab.IssueEvent) error {
	_, _, _, err := h.SlackClient.SendMessageContext(
		ctx, h.ChannelID,
		slack.MsgOptionText(
			fmt.Sprintf(
				"Issue '%s' %sed: <%s>", issue.ObjectAttributes.Title, issue.ObjectAttributes.Action,
				issue.ObjectAttributes.URL,
			),
			false,
		),
	)
	return err
}

func labelsInclude(labels []gogitlab.Label, label string) bool {
	for _, existingLabel := range labels {
		if existingLabel.Name == label {
			return true
		}
	}
	return false
}
