package gitlab

import (
	"bytes"
	"context"
	"log"
	"os"
	"os/exec"
	"strings"
	"text/template"

	gogitlab "github.com/xanzy/go-gitlab"
)

func NotifyMergeRequestFromMirrorSource(ctx context.Context, client *gogitlab.Client, logger *log.Logger, mirrorProjectPath, pipelineStatus string) error {
	projPath := mirrorProjectPath
	if projPath == "" {
		projPath = os.Getenv("CI_PROJECT_PATH")
	}
	logger.Println("Using project path " + projPath)

	// There are a bunch of assumptions about our workflow encoded here:
	//
	// - A non-master commit has exactly one parent
	// - A master commit can have >=1 parents (usually exactly 1)
	// - We create merge requests for non-master commits against master

	commits := []string{os.Getenv("CI_COMMIT_SHA")}
	if os.Getenv("CI_COMMIT_BRANCH") == "master" {
		parentsOutput, err := exec.Command("git", "rev-list", "--parents", "-n1", "HEAD").CombinedOutput()
		if err != nil {
			logger.Println("error: " + err.Error())
			return err
		}
		commits = strings.Split(strings.TrimSpace(string(parentsOutput)), " ")
	}

	var mrs []*gogitlab.MergeRequest

	for _, commit := range commits {
		commitMrs, _, err := client.Commits.GetMergeRequestsByCommit(projPath, commit, gogitlab.WithContext(ctx))
		if err != nil {
			logger.Println("error: " + err.Error())
			return err
		}
		mrs = append(mrs, commitMrs...)
	}

	noteTemplate, err := template.New("note").Parse(noteBodyTemplate)
	if err != nil {
		logger.Println("error: " + err.Error())
		return err
	}
	var noteBodyBuf bytes.Buffer
	err = noteTemplate.Execute(&noteBodyBuf, struct {
		URL, Status string
	}{
		URL: os.Getenv("CI_PIPELINE_URL"), Status: pipelineStatus,
	})
	if err != nil {
		logger.Println("error: " + err.Error())
		return err
	}
	noteBody := noteBodyBuf.String()

	for _, mr := range mrs {
		logger.Printf("Linking MR %s to this pipeline.", mr.WebURL)
		_, _, err := client.Notes.CreateMergeRequestNote(projPath, mr.IID, &gogitlab.CreateMergeRequestNoteOptions{Body: &noteBody}, gogitlab.WithContext(ctx))
		if err != nil {
			logger.Println("error: " + err.Error())
			return err
		}
	}
	return nil
}

const noteBodyTemplate = `A pipeline is running on a mirror related to this merge request.

Status: {{ .Status }}

{{ .URL }}
`
