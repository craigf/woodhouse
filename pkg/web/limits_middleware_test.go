package web_test

import (
	"context"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/web"
)

func TestLimitsMiddleware_callsTheNextHandler(t *testing.T) {
	handlerCallCount := 0
	handler := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		handlerCallCount++
		_, err := ioutil.ReadAll(req.Body)
		require.Nil(t, err)
	})
	middleware := web.NewLimitsMiddleware(time.Second, 1024*1024, 100, handler)
	server := httptest.NewServer(middleware)
	defer server.Close()

	req, err := http.NewRequest(http.MethodPost, server.URL, strings.NewReader("small body"))
	require.Nil(t, err)
	resp, err := server.Client().Do(req)
	require.Nil(t, err)
	defer resp.Body.Close()
	assert.Equal(t, 200, resp.StatusCode)

	assert.Equal(t, 1, handlerCallCount)
}

func TestLimitsMiddleware_errorsWhenBodyOversized(t *testing.T) {
	handlerCallCount := 0
	handler := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		handlerCallCount++
		body, err := ioutil.ReadAll(req.Body)
		assert.Equal(t, 5, len(body))
		if err != nil {
			w.WriteHeader(http.StatusRequestEntityTooLarge)
			return
		}
	})
	middleware := web.NewLimitsMiddleware(time.Second, 5, 100, handler)
	server := httptest.NewServer(middleware)
	defer server.Close()

	req, err := http.NewRequest(http.MethodPost, server.URL, strings.NewReader("small body, but still too large"))
	require.Nil(t, err)
	resp, err := server.Client().Do(req)
	require.Nil(t, err)
	defer resp.Body.Close()
	assert.Equal(t, 413, resp.StatusCode)

	assert.Equal(t, 1, handlerCallCount)
}

func TestLimitsMiddleware_cancelsContextAfterTimeout(t *testing.T) {
	handlerCallCount := 0
	handler := http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		handlerCallCount++
		longRunningOperationResult := time.After(time.Second)
		select {
		case <-longRunningOperationResult:
			w.WriteHeader(http.StatusOK)
		case <-req.Context().Done():
			assert.EqualError(t, req.Context().Err(), context.DeadlineExceeded.Error())
			w.WriteHeader(http.StatusRequestTimeout)
		}
	})
	middleware := web.NewLimitsMiddleware(time.Nanosecond, 5, 100, handler)
	server := httptest.NewServer(middleware)
	defer server.Close()

	req, err := http.NewRequest(http.MethodPost, server.URL, strings.NewReader("small body"))
	require.Nil(t, err)
	resp, err := server.Client().Do(req)
	require.Nil(t, err)
	defer resp.Body.Close()
	assert.Equal(t, 408, resp.StatusCode)

	assert.Equal(t, 1, handlerCallCount)
}
