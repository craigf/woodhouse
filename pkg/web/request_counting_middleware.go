package web

import (
	"net/http"
	"sync"
)

func NewRequestCountingMiddleware(requestGauge *sync.WaitGroup, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		requestGauge.Add(1)
		defer requestGauge.Done()
		next.ServeHTTP(w, req)
	})
}
