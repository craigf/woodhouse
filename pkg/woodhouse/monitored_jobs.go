package woodhouse

import (
	"context"
	"fmt"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/prometheus/client_golang/prometheus/push"
)

type JobMonitor struct {
	startTime   *prometheus.GaugeVec
	successTime *prometheus.GaugeVec
	maxAge      *prometheus.GaugeVec
	failed      *prometheus.GaugeVec
	pusher      *push.Pusher
}

func NewJobMonitor(registry *prometheus.Registry, pushGatewayURL string) *JobMonitor {
	monitor := &JobMonitor{}

	metrics := promauto.With(registry)
	monitor.startTime = metrics.NewGaugeVec(prometheus.GaugeOpts{
		Name: "gitlab_job_start_timestamp_seconds", Help: "The start time of the job.",
	}, []string{"resource"})
	monitor.successTime = metrics.NewGaugeVec(prometheus.GaugeOpts{
		Name: "gitlab_job_success_timestamp_seconds", Help: "The time the job succeeded.",
	}, []string{"resource"})
	monitor.maxAge = metrics.NewGaugeVec(prometheus.GaugeOpts{
		Name: "gitlab_job_max_age_seconds", Help: "How long the job is allowed to run before marking it failed.",
	}, []string{"resource"})
	monitor.failed = metrics.NewGaugeVec(prometheus.GaugeOpts{
		Name: "gitlab_job_failed", Help: "Boolean status of the job.",
	}, []string{"resource"})

	monitor.pusher = push.New(pushGatewayURL, "woodhouse").Gatherer(registry)

	return monitor
}

func (j *JobMonitor) Run(ctx context.Context, jobName string, maxAge time.Duration, job Job) error {
	startTime := j.startTime.WithLabelValues(jobName)
	startTime.Set(float64(time.Now().UTC().Unix()))
	j.maxAge.WithLabelValues(jobName).Set(float64(maxAge / time.Second))
	if err := j.pusher.Push(); err != nil {
		fmt.Printf("error pushing metrics: %s\n", err)
	}

	err := job(ctx)

	if err == nil {
		j.successTime.WithLabelValues(jobName).Set(float64(time.Now().UTC().Unix()))
	} else {
		j.failed.WithLabelValues(jobName).Set(1)
	}
	if err := j.pusher.Push(); err != nil {
		fmt.Printf("error pushing metrics: %s\n", err)
	}

	return err
}
