package woodhouse

import (
	"context"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
	"gitlab.com/gitlab-com/gl-infra/woodhouse/pkg/instrumentation"
)

type Job func(ctx context.Context) error

type AsyncJobRunner struct {
	JobTimeout  time.Duration
	Logger      log.Logger
	ThreadCount *sync.WaitGroup
}

func (r *AsyncJobRunner) RunAsyncJob(job Job) {
	ctx, cancel := context.WithTimeout(context.Background(), r.JobTimeout)
	event := &instrumentation.Event{}
	ctx = context.WithValue(ctx, instrumentation.EventContextKey{}, event)
	r.ThreadCount.Add(1)

	go func() {
		defer r.ThreadCount.Done()
		defer cancel()

		var err error
		duration := instrumentation.MeasureTime(func() {
			err = job(ctx)
		})
		if err == nil {
			event.With("status", "success")
		} else {
			event.With("status", "error", "error", err)
		}

		event.With("event", "async_job_complete", "duration_s", duration)
		r.Logger.Log(*event...)
	}()
}
